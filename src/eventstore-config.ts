import { configNode } from '@alls/node-red-common'
import { Node } from 'node-red'

interface Config {
	connectionString: string
}

interface Credentials {
	user: string
}

export type EventStoreConfigNode = Node<Credentials> & Config

export default configNode<Config, Credentials>('eventstore-config',
	(node, config) => {
		node.connectionString = config.connectionString
			|| 'esdb://localhost:2113?tls=false'
	},
	{ user: { type: 'text' } }
)
