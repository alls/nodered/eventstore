"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_red_common_1 = require("@alls/node-red-common");
exports.default = (0, node_red_common_1.configNode)('eventstore-config', (node, config) => {
    node.connectionString = config.connectionString
        || 'esdb://localhost:2113?tls=false';
}, { user: { type: 'text' } });
