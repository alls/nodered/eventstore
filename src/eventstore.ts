import { customNode, errorMessage } from '@alls/node-red-common'
import {
	AppendExpectedRevision, EventStoreDBClient, EventType, FORWARDS, jsonEvent,
	JSONType, ResolvedEvent, START, StreamingRead, StreamSubscription
} from '@eventstore/db-client'
import { EventStoreConfigNode } from './eventstore-config'

declare global {
	interface BigInt {
		toJSON: () => string
	}
}
BigInt.prototype.toJSON = function () { return this.toString() }

interface Config {
	access: string
	// TODO: separate nodes instead of 'mode' with specific
	//  config and msg arguments.
	mode?: 'read' | 'append' | 'subscribe'
	streamId?: string
	eventType?: string
	batchSize?: number
}

interface EventDataInput {
	type: string,
	data: JSONType,
	revision?: AppendExpectedRevision,
	streamId?: string
}

interface Message {
	streamId?: string
	eventType?: string
	expectedRevision?: AppendExpectedRevision
	payload: EventDataInput | JSONType | (EventDataInput | JSONType)[]
}

export default customNode<Message, Config>('eventstore',
	(node, config, api) => {
		const access = api.nodes.getNode(config.access) as EventStoreConfigNode
		const mode = config.mode || 'read'
		let db: EventStoreDBClient
		let subscription: StreamSubscription
		const appendJob: Record<string, Promise<void>> = {}

		return {
			onInput: async (msg) => {
				const streamId = config.streamId || msg.streamId || 'default-stream'
				const type = config.eventType || msg.eventType
				try {
					if (!db)
						db = EventStoreDBClient.connectionString(access.connectionString)
					if (mode === 'append') {
						if (!msg.payload)
							throw new Error('Cannot append empty payload!')
						const event = Array.isArray(msg.payload)
							? msg.payload.map(data => type
								? jsonEvent({ type, data: data as JSONType })
								: jsonEvent(data as EventDataInput))
							: type
								? jsonEvent({ type, data: msg.payload as JSONType })
								: jsonEvent(msg.payload as EventDataInput)
						const firstEvent =
							Array.isArray(msg.payload) ? msg.payload[0] : msg.payload
						const firstEventObject: EventDataInput | null =
							typeof firstEvent === 'object'
								? firstEvent as EventDataInput : null
						const streamId = firstEventObject?.streamId
							?? (config.streamId || msg.streamId || 'default-stream')
						const expectedRevision =
							firstEventObject?.revision ?? msg.expectedRevision
						const options = expectedRevision ? { expectedRevision } : void 0

						const job = async () => {
							node.status.show(
								`appending to ${streamId} (${access.connectionString})`)
							const result = await db.appendToStream(streamId, event, options)
							node.status.show(JSON.stringify(result))
							node.send({ ...msg, payload: result })
						}
						if (streamId in appendJob)
							await appendJob[streamId]
						appendJob[streamId] = job()
						await appendJob[streamId]
					} else if (mode === 'subscribe') {
						if (subscription)
							await subscription.unsubscribe()
						if (msg.payload) {
							node.status.show(
								`listening to ${streamId} (${access.connectionString})`)
							subscription = db.subscribeToStream(streamId,
								{ fromRevision: START, resolveLinkTos: true })
							for await (const { event } of subscription) {
								if (event) {
									const evn = { ...event, revision: Number(event.revision) }
									if (!event.isJson)
										evn.data = new TextDecoder().decode(event.data)
									console.log(evn.revision, evn.type, JSON.stringify(evn.data))
									node.send({ ...msg, payload: evn })
								}
							}
						}
					} else if (mode === 'read') {
						node.status.show(
							`reading from ${streamId} (${access.connectionString})`)
						await sendEvents(db.readStream(streamId, {
							// TODO: allow to configure/msg read direction,
							// fromRevision, maxCount
							direction: FORWARDS, fromRevision: START,
						}))
					} else if (mode === 'read-all') {
						node.status.show(`reading from $all (${access.connectionString})`)
						await sendEvents(db.readAll({
							direction: FORWARDS, fromPosition: START,
						}))
					}
				}
				catch (err) {
					throw new Error(`${errorMessage(err)} (${access.connectionString})`)
				}

				async function sendEvents(
					events: StreamingRead<ResolvedEvent<EventType>>) {
					const batchSize = config.batchSize || 1
					if (batchSize > 1) {
						const batch = []
						for await (const { event } of events) {
							if (event?.isJson)
								batch.push(event)
							if (batch.length >= batchSize) {
								node.send({ ...msg, payload: batch })
								batch.length = 0
							}
						}
						if (batch.length > 0)
							node.send({ ...msg, payload: batch })
					} else {
						for await (const { event } of events) {
							if (event?.isJson)
								node.send({ ...msg, payload: event })
						}
					}
				}
			}
		}

	}
)
