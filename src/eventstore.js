"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const node_red_common_1 = require("@alls/node-red-common");
const db_client_1 = require("@eventstore/db-client");
BigInt.prototype.toJSON = function () { return this.toString(); };
exports.default = (0, node_red_common_1.customNode)('eventstore', (config, status, api) => {
    const access = api.nodes.getNode(config.access);
    const mode = config.mode || 'read';
    let db;
    let subscription;
    const appendJob = {};
    return async (msg, send) => {
        var _a, _b;
        const streamId = config.streamId || msg.streamId || 'default-stream';
        const type = config.eventType || msg.eventType;
        try {
            if (!db)
                db = db_client_1.EventStoreDBClient.connectionString(access.connectionString);
            if (mode === 'append') {
                if (!msg.payload)
                    throw new Error('Cannot append empty payload!');
                const event = Array.isArray(msg.payload)
                    ? msg.payload.map(data => type
                        ? (0, db_client_1.jsonEvent)({ type, data: data })
                        : (0, db_client_1.jsonEvent)(data))
                    : type
                        ? (0, db_client_1.jsonEvent)({ type, data: msg.payload })
                        : (0, db_client_1.jsonEvent)(msg.payload);
                const firstEvent = Array.isArray(msg.payload) ? msg.payload[0] : msg.payload;
                const firstEventObject = typeof firstEvent === 'object' ? firstEvent : null;
                const streamId = (_a = firstEventObject === null || firstEventObject === void 0 ? void 0 : firstEventObject.streamId) !== null && _a !== void 0 ? _a : (config.streamId || msg.streamId || 'default-stream');
                const expectedRevision = (_b = firstEventObject === null || firstEventObject === void 0 ? void 0 : firstEventObject.revision) !== null && _b !== void 0 ? _b : msg.expectedRevision;
                const options = expectedRevision ? { expectedRevision } : void 0;
                const job = async () => {
                    status.show(`appending to ${streamId} (${access.connectionString})`);
                    const result = await db.appendToStream(streamId, event, options);
                    status.show(JSON.stringify(result));
                    send({ ...msg, payload: result });
                };
                if (streamId in appendJob)
                    await appendJob[streamId];
                appendJob[streamId] = job();
                await appendJob[streamId];
            }
            else if (mode === 'subscribe') {
                if (subscription)
                    await subscription.unsubscribe();
                if (msg.payload) {
                    status.show(`listening to ${streamId} (${access.connectionString})`);
                    subscription = db.subscribeToStream(streamId, { fromRevision: db_client_1.START, resolveLinkTos: true });
                    for await (const { event } of subscription) {
                        if (event) {
                            const evn = { ...event, revision: Number(event.revision) };
                            if (!event.isJson)
                                evn.data = new TextDecoder().decode(event.data);
                            console.log(evn.revision, evn.type, JSON.stringify(evn.data));
                            send({ ...msg, payload: evn });
                        }
                    }
                }
            }
            else if (mode === 'read') {
                status.show(`reading from ${streamId} (${access.connectionString})`);
                await sendEvents(db.readStream(streamId, {
                    // TODO: allow to configure/msg read direction,
                    // fromRevision, maxCount
                    direction: db_client_1.FORWARDS, fromRevision: db_client_1.START,
                }));
            }
            else if (mode === 'read-all') {
                status.show(`reading from $all (${access.connectionString})`);
                await sendEvents(db.readAll({
                    direction: db_client_1.FORWARDS, fromPosition: db_client_1.START,
                }));
            }
        }
        catch (err) {
            throw new Error(`${(0, node_red_common_1.errorMessage)(err)} (${access.connectionString})`);
        }
        async function sendEvents(events) {
            const batchSize = config.batchSize || 1;
            if (batchSize > 1) {
                const batch = [];
                for await (const { event } of events) {
                    if (event === null || event === void 0 ? void 0 : event.isJson)
                        batch.push(event);
                    if (batch.length >= batchSize) {
                        send({ ...msg, payload: batch });
                        batch.length = 0;
                    }
                }
                if (batch.length > 0)
                    send({ ...msg, payload: batch });
            }
            else {
                for await (const { event } of events) {
                    if (event === null || event === void 0 ? void 0 : event.isJson)
                        send({ ...msg, payload: event });
                }
            }
        }
    };
});
